#!/usr/bin/env groovy
def call(Map stepParams){
    checkout()
    readConfigFile(configFilePath: "${stepParams.configFilePath}")
    ansibleLinting()
    publishReport()
    runPlaybook()
    notificationStage()
    commonUtils.cleanWorkspace()
}

def checkout(){
    try {
        vcsUtils.checkoutCode()
    }
    catch(Exception e) {
        notification.sendErrorInfo(
            slack_channel:  "${config.SLACK_CHANNEL_NAME}",
            color:          "danger",
            messsage:       "while clonning the codebase",
            environment:    "${config.ENVIRONMENT}",
            error:          e
        )
    }
}

def readConfigFile(Map stepParams){
    try {
        stage("Reading config file"){
            config = commonUtils.readPropertyFile(
                configFilePath: "${stepParams.configFilePath}"
            )
        }
    }
    catch(Exception e) {
        notification.sendErrorInfo(
            slack_channel:  "${config.SLACK_CHANNEL_NAME}",
            color:          "danger",
            messsage:       "fail reading config file",
            environment:    "${config.ENVIRONMENT}",
            error:          e
        )
    }
}

def notificationStage() {
    stage("Sending Notification") {
        notification.sendInfoMessage(
            slack_channel:   "${config.SLACK_CHANNEL_NAME}",
            environment:    "${config.ENVIRONMENT}",
            message:        "Successfully ${config.ACTION_MESSAGE}"
        )
    }
}

def ansibleLinting(playbook){
   try {
       if ("${config.LINTING}" == "true"){
            stage("Playbook Linting"){
                ansible.lintingOperation(
                    codePath:   "${config.CODE_PATH}",
                    playbook:   "${config.PLAYBOOK_NAME}"
                )
            }
        }
    }
    catch(Exception e) {
        notification.sendErrorInfo(
            slack_channel:  "${config.SLACK_CHANNEL_NAME}",
            color:          "danger",
            messsage:       "fail in Linting code",
            environment:    "${config.ENVIRONMENT}",
            error:          e
        )
    }
}

def publishReport(){
    if ("${config.LINTING}" == "true"){
        stage("Publish Linting Report"){
            junit testResults: "${config.CODE_PATH}/ansible-lint.xml"
        }
    }
}

def runPlaybook(){
   try {
       stage("${config.ACTION_MESSAGE}"){
            if ("${env.BRANCH_NAME}" == "${config.BRANCH}"){
                if ("${config.EXTRA_ARGS}" == "null"){
                    ansible.executeOperation(
                        codePath:   "${config.CODE_PATH}",
                        inventory:  "${config.INVENTORY}",
                        playbook:   "${config.PLAYBOOK_NAME}"
                    )
                }
                else {
                    ansible.executeOperation(
                        codePath:   "${config.CODE_PATH}",
                        inventory:  "${config.INVENTORY}",
                        playbook:   "${config.PLAYBOOK_NAME}",
                        extra_args: " ${config.EXTRA_ARGS}"
                    )
                }
            }
            else {
                echo "this branch ${env.BRANCH_NAME} can not run ${config.ACTION_MESSAGE}"
                break
            }
        }
    }
    catch(Exception e) {
        notification.sendErrorInfo(
            slack_channel:  "${config.SLACK_CHANNEL_NAME}",
            color:          "danger",
            messsage:       "fail to ${config.ACTION_MESSAGE} ",
            environment:    "${config.ENVIRONMENT}",
            error:          e
        )
    }
}
