#!/usr/bin/env groovy

def executeOperation(Map setpParams){
    if ("${setpParams.extra_args}" == null){
        sh "ansible-playbook -i ${setpParams.inventory} ${setpParams.codePath}/${setpParams.playbook}"   
    }
    else{
        sh "ansible-playbook -i ${setpParams.inventory} ${setpParams.codePath}/${setpParams.playbook} ${setpParams.extra_args}"   
    } 
}

def lintingOperation(Map stepParams) {
   dir("${stepParams.codePath}") {
        sh script: "ansible-lint ${stepParams.playbook} -p | ansible-lint-junit -o ansible-lint.xml" 
   }
}
