#!/usr/bin/env groovy

def sendSlackNotification(Map stepParams) {
    slackSend channel: "${stepParams.slackChannel}",
    color: "${stepParams.buildStatus}",
    message: "ENVIRONMENT:- ${stepParams.environment}\n JOB_NAME:- ${env.JOB_NAME}\n BUILD_URL:- ${env.BUILD_URL}\n MESSAGE:- ${stepParams.message}"
}

def sendEmailNotification(Map stepParams) {
    emailext body: "${stepParams.environment}\n BUILD_ID:- ${env.BUILD_ID}\n JOB_NAME:- ${env.JOB_NAME}\n BUILD_URL:- ${env.BUILD_URL}|BUILD_URL>\n Message:- ${stepParams.message}",
    recipientProviders: [developers()],
    subject: 'satus', to: "${stepParams.mail_id}"
}

def sendErrorInfo(Map stepParams){
    send_msg = "${stepParams.message}\n ${stepParams.error.toString()}"
    sendSlackNotification(
            slackChannel:   "${stepParams.slack_channel}",
            buildStatus:    "${stepParams.color}",
            environment:    "${stepParams.environment}",
            message:        "${send_msg}"
        )
}

def sendInfoMessage(Map stepParams){
    sendSlackNotification(
        slackChannel:   "${stepParams.slack_channel}",
        buildStatus:    "good",
        environment:    "${stepParams.environment}",
        message:        "${stepParams.message}"
    )
}

def successNotificationStage(Map stepParams) {
    stage("Sending success notification") {
        sendSlackNotification(
            slackChannel: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}",
            buildStatus: "good",
            message: "${stepParams.message}"
        )  
    }
}

def slackMessage(Map stepParams){
    def msg = ""
    stepParams.each {
        msg += "$it.key: $it.value\n" 
    }
    return msg
}