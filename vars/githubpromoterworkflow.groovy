#!/usr/bin/env groovy
def call(Map stepParams){
    def config = [:]
    vcsUtils.checkoutCode()

    config = commonUtils.readConfigFileStage(
        configFilePath: "${stepParams.configFilePath}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )

    commit_detail = vcsUtils.commitDetails()

    msg = vcsUtils.promoteToGitHub(
        github_url: "${config.GITHUB_URL}",
        token: "${config.GITHUB_TOKEN_NAME}",
        token_user: "${config.GITHUB_TOKEN_USER}",
        github_promote: "${config.PROMOTE}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )
    if(msg == 'success'){
        slack_message = "Successfully Sync gitlab master branch with github\n"
    }
    else{
        slack_message = "Branch not allowed to promote\n"
    }

    slack_message += notification.slackMessage(
        ROLE_NAME: "${config.REPO}",
        GITLAB_URL: "${scm.userRemoteConfigs[0].url}",
        BRANCH: "${env.BRANCH_NAME}",
        GITHUB_URL: "https://${config.GITHUB_URL}",
        LAST_COMMITER: "${commit_detail.commiter_name}",
        COMMIT_MSG: "${commit_detail.commit_msg}"
    )

    notification.successNotificationStage(
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}",
        message: slack_message
    )
    
    commonUtils.cleanWorkspace()  
}

