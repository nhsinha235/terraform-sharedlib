#!/usr/bin/env groovy

def initializeterraforms(Map stepParams) {
    stage("Initializing the terraform modules") {
        if("${stepParams.extraArguments}" != "null") {
            terraform.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "init -no-color ${stepParams.extraArguments}"
            )
        } else {
            terraform.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "init -no-color"
            )
        }
    }
}

def lintTerraformCode(Map stepParams) {
    stage("Linting terraform code") {
        terraform.executeOperation(
            codePath: "${config.CODE_BASE_PATH}",
            operation: "fmt -list=true -write=false -diff=true"
        )
    }
}

def planInfrastructure(Map stepParams) {
    stage("Planning ${stepParams.actionMessage}") {
        if("${stepParams.extraArguments}" != "null") {
            terraform.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "plan ${stepParams.extraArguments}"
            )
        } else {
            terraform.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "plan"
            )
        }
    }
}

def createInfrastructure(Map stepParams) {
    stage("Applying ${stepParams.actionMessage}") {
        if("${stepParams.extraArguments}" != "null") {
            terraform.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "apply -auto-approve ${stepParams.extraArguments}"
            )
        } else {
            terraform.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "apply -auto-approve"
            )
        }
    }
}

def runTestCases(Map stepParams) {
    stage("Executing test cases for created infrastructure") {
        inspec.executeTestCases(
            testCasePath: "${config.CODE_BASE_PATH}",
            command: "exec . -t azure:// --no-color --chef-license=accept"
        )
    }
}

def sendClusterSuccessNotification(Map stepParams) {
    stage("Sending success notification on slack") {
        notification.sendSlackNotification(
            slackChannel: "${stepParams.channelName}",
            buildStatus: "good",
            environment: "${config.ENVIRONMENT}",
            message: "${stepParams.message}"
        )
    }
}

def sendClusterFailNotification(Map stepParams) {
    stage("Sending failure notification on slack") {
        notification.sendSlackNotification(
            slackChannel: "${stepParams.channelName}",
            buildStatus: "danger",
            environment: "${config.ENVIRONMENT}",
            message: "${stepParams.message}"
        )
    }
}

def call(Map stepParams) {
    
    try {
        vcsUtils.checkoutCode()
    } catch (Exception e) {
        echo "Failed while clonning the codebase"
        echo e.toString()
        throw e
    }

    try {
        config = commonUtils.readPropertyFile(
            configFilePath: "${stepParams.configFilePath}"
        )
    } catch (Exception e) {
        echo "Failed while reading config file"
        echo e.toString()
        throw e
    }

    try {
        lintTerraformCode(
            codeBasePath: "${config.CODE_BASE_PATH}"
        )
    } catch (Exception e) {
        echo "Failed while linting code"
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while linting code"
        )
        echo e.toString()
        throw e
    }
    
    try {
        initializeterraforms(
            codeBasePath: "${config.CODE_BASE_PATH}",
            extraArguments: "${config.EXTRA_ARGS}"
        )
    } catch (Exception e) {
        echo "Failed while initializing terraform modules"
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while initializing terraform modules"
        )
        echo e.toString()
        throw e
    }

    try {
        planInfrastructure(
            codeBasePath: "${config.CODE_BASE_PATH}",
            extraArguments: "${config.EXTRA_ARGS}",
            actionMessage: "${config.ACTION_MESSAGE}"
        )
    } catch (Exception e) {
        echo "Failed while planning infrastructure"
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while planning ${config.ACTION_MESSAGE}"
        )
        echo e.toString()
        throw e
    }

    if("${env.BRANCH_NAME}" == "master") {

        if("${config.KEEP_APPROVAL_STAGE}" == "true" || "${config.KEEP_APPROVAL_STAGE}" == "null") {
            commonUtils.approvalStep()
        }

        try {
            createInfrastructure(
                codeBasePath: "${config.CODE_BASE_PATH}",
                extraArguments: "${config.EXTRA_ARGS}",
                actionMessage: "${config.ACTION_MESSAGE}"
            )
        } catch (Exception e) {
            echo "Failed while creating infrastructure"
            sendClusterFailNotification(
                channelName: "${config.SLACK_CHANNEL_NAME}",
                environment: "${config.ENVIRONMENT}",
                message: "Failed while applying ${config.ACTION_MESSAGE}"
            )
            echo e.toString()
            throw e
        }

        if("${config.ENABLE_TEST_CASE}" == "true") {
            try {
                runTestCases(
                    codeBasePath: "${config.TEST_CASE_PATH}"
                )
            } catch (Exception e) {
                echo "Failed while executing test cases"
                sendClusterFailNotification(
                    channelName: "${config.SLACK_CHANNEL_NAME}",
                    environment: "${config.ENVIRONMENT}",
                    message: "Failed while executing test cases"
                )
                echo e.toString()
                throw e
            }
        }

        sendClusterSuccessNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Successfully applied ${config.ACTION_MESSAGE}"   
        )

    } else {
        echo "Skipping execution because of non-master branch!!!!"
    }
}
