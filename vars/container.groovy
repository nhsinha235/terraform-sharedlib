#!/usr/bin/env groovy

def imageLinting(Map stepParams) {
    sh "docker run --rm -i -v ${WORKSPACE}:/dockerlinter \
        quay.io/opstree/dockerlinter:v1.0 ${stepParams.dockerfilePath}"

    publishHTML([
        allowMissing: false,
        alwaysLinkToLastBuild: false,
        keepAll: false,
        reportDir: "",
        reportFiles: 'result.html',
        reportName: "Docker Linting Report",
        reportTitles: ''
    ])
}

def buildImage(Map stepParams) {
    def image_name = "${stepParams.imageName}"
    def image_tag = "${stepParams.imageTag}"
    sh "docker build -t ${image_name}:${image_tag} -f ${stepParams.dockerfilePath} ."
}

def scanImage(Map stepParams) {
    def image_name = "${stepParams.imageName}"
    def image_tag = "${stepParams.imageTag}"
    aquaMicroscanner imageName: "${image_name}:${image_tag}", 
    notCompliesCmd: '', 
    onDisallowed: 'ignore', 
    outputFormat: 'html'
}

def pushImage(Map stepParams) {
    def image_name = "${stepParams.imageName}"
    def image_tag = "${stepParams.imageTag}"
    withDockerRegistry(credentialsId: "${stepParams.registryCredential}", url: "${stepParams.registryLoginURL}") {
        sh "docker tag ${image_name}:${image_tag} ${stepParams.registryURL}/${image_name}:${image_tag}"
        sh "docker tag ${stepParams.registryURL}/${image_name}:${image_tag} ${stepParams.registryURL}/${image_name}:latest"
        sh "docker push ${stepParams.registryURL}/${image_name}:${image_tag}"
        sh "docker push ${stepParams.registryURL}/${image_name}:latest"
    }
}

def cleanUpImage(Map stepParams) {
    def image_name = "${stepParams.imageName}"
    def image_tag = "${stepParams.imageTag}"
    sh "docker rmi -f ${stepParams.registryURL}/${image_name}:${image_tag} ${stepParams.registryURL}/${image_name}:latest"
}

def buildDockerImageStage(Map stepParams) {
    try {
        stage("Building docker image for ${stepParams.imageName}") {
            buildImage(
                imageName: "${stepParams.imageName}",
                imageTag: "${stepParams.imageTag}",
                dockerfilePath: "${stepParams.dockerfilePath}"
            )
        }
    }
    catch(Exception e) {
        notification.sendSlackNotification(
            slackChannel: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}",
            buildStatus: "danger",
            message: "Job failed while building docker image\n IMAGE_NAME:- ${stepParams.imageName}"
        )
        throw e
        echo e.tostring()
    }
}

def checkImageLintStage(Map stepParams) {
    if ("${stepParams.checkLint}" != "false") {
        try {
            stage("Checking dockerfile linting for ${stepParams.imageName}") {
                imageLinting(
                    dockerfilePath: "${stepParams.dockerfilePath}"
                )
            }
        }
        catch(Exception e) {
            notification.sendSlackNotification(
                slackChannel: "${stepParams.slackChannelName}",
                environment: "${stepParams.environment}",
                buildStatus: "danger",
                message: "Job failed while linting docker image\n IMAGE_NAME:- ${stepParams.imageName}"
            )
            throw e
            echo e.tostring()
        }
    }
}

def scanDockerImageStage(Map stepParams) {
    if ("${stepParams.checkScan}" != "false") {
        try {
            stage("Scanning docker image for ${stepParams.imageName}") {
                scanImage(
                    imageName: "${stepParams.imageName}",
                    imageTag: "${stepParams.imageTag}"
                )
            }
        }
        catch(Exception e) {
            notification.sendSlackNotification(
                slackChannel: "${stepParams.slackChannelName}",
                environment: "${stepParams.environment}",
                buildStatus: "danger",
                message: "Job failed while scanning docker image\n IMAGE_NAME:- ${stepParams.imageName}"
            )
            throw e
            echo e.tostring()
        }   
    }
}

def publishDockerImageStage(Map stepParams) {
    try {
        stage("Publishing docker image for ${stepParams.imageName}") {
            if ("${stepParams.publishDockerHub}" == "true") {
                pushImage(
                    imageName: "${stepParams.imageName}",
                    imageTag: "${stepParams.imageTag}",
                    registryLoginURL: "https://index.docker.io/v1/",
                    registryURL: "${stepParams.dockerHubUsername}",
                    registryCredential: "${stepParams.dockerHubCredential}"
                )
            }

            if ("${stepParams.publishQuay}" == "true") {
                pushImage(
                    imageName: "${stepParams.imageName}",
                    imageTag: "${stepParams.imageTag}",
                    registryLoginURL: "https://${stepParams.quayURL}",
                    registryURL: "${stepParams.quayURL}/${stepParams.quayUsername}",
                    registryCredential: "${stepParams.quayCredential}"
                )
            }
        }
    }
    catch(Exception e) {
        notification.sendSlackNotification(
            slackChannel: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}",
            buildStatus: "danger",
            message: "Job failed while pushing docker image\n IMAGE_NAME:- ${stepParams.imageName}"
        )
        throw e
        echo e.tostring()
    }
}

def imageCleanUpStage(Map stepParams) {
    try {
        stage("Cleaning up docker image") {
            cleanUpImage(
                imageName: "${stepParams.imageName}",
                imageTag: "${stepParams.imageTag}"
            )
        }
    }
    catch(Exception e) {
        notification.sendSlackNotification(
            slackChannel: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}",
            buildStatus: "danger",
            message: "Job failed while cleaning up docker image\n IMAGE_NAME:- ${stepParams.imageName}"
        )
        throw e
        echo e.tostring()
    }
}
