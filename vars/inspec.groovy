#!/usr/bin/env groovy

def executeTestCases(Map stepParams) {
    dir("${stepParams.testCasePath}") {
        sh "inspec ${stepParams.command}"
    }
}
