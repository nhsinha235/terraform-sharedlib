#!/usr/bin/env groovy

def call(Map stepParams) {
    def config = [:]
    vcsUtils.checkoutCode()
    config = commonUtils.readConfigFileStage(
        configFilePath: "${stepParams.configFilePath}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )

    container.checkImageLintStage(
        imageName: "${config.IMAGE_NAME}",
        checkLint: "${config.CHECK_LINT}",
        dockerfilePath: "${config.DOCKERFILE_PATH}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}" 
    )

    container.buildDockerImageStage(
        imageName: "${config.IMAGE_NAME}",
        imageTag: "${config.IMAGE_VERSION}",
        dockerfilePath: "${config.DOCKERFILE_PATH}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )

    container.scanDockerImageStage(
        imageName: "${config.IMAGE_NAME}",
        imageTag: "${config.IMAGE_VERSION}",
        checkScan: "${config.SCAN_IMAGE}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )

    container.publishDockerImageStage(
        imageName: "${config.IMAGE_NAME}",
        imageTag: "${config.IMAGE_VERSION}",
        publishDockerHub: "${config.PUBLISH_DOCKERHUB}",
        publishQuay: "${config.PUBLISH_QUAY}",
        dockerHubUsername: "${config.DOCKERHUB_USERNAME}",
        dockerHubCredential: "${config.DOCKERHUB_CREDENTIAL}",
        quayURL: "${config.QUAY_URL}",
        quayUsername: "${config.QUAY_USERNAME}",
        quayCredential: "${config.QUAY_CREDENTIAL}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )

    container.imageCleanUpStage(
        imageName: "${config.IMAGE_NAME}",
        imageTag: "${config.IMAGE_VERSION}",
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}"
    )

    notification.successNotificationStage(
        slackChannelName: "${config.SLACK_CHANNEL_NAME}",
        environment: "${config.ENVIRONMENT}",
        message: "Successfully build and published docker image\n IMAGE_NAME:- ${config.IMAGE_NAME}"
    )
}
