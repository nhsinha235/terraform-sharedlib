### docker-ci-v0.1-latest (Docker workflow release)

##### April 21, 2020

#### :tada: Features

- Added Docker image publishing worflow
- Capabilities:-
    - Image building
    - Dockerfile linting
    - Image Scanning
    - Dockerhub and Quay registry support

### docker-ci-v0.1.1 (Docker workflow tag and cleanup)

##### May 8, 2020

#### :tada: Features

- Updated library to support custom tags only
- Added image cleanup stage to remove pushed images
