package org.opstree.commonutilities
def approvalStep() {
    stage("Waiting for Approval") {
        input 'Do you want to proceed or not?'
    }
}

def readPropertyFile(Map stepParams) {
    config = readProperties file: "${stepParams.configFilePath}"
    return config
}

def cleanWorkspace(){
    stage("Clean WorkSpace"){
        cleanWs()
    }
}

def readConfigFileStage(Map stepParams){
    try {
        stage("Reading configuration file"){
            config = readProperties file: "${stepParams.configFilePath}"
        }
    }
    catch(Exception e) {
        notification.sendSlackNotification(
            slackChannel: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}",
            buildStatus: "danger",
            message: "Job failed while reading property file"
        )
       
        echo e.tostring()
        throw e
    }
    return config
}
