package org.opstree.commonutilities

def checkoutCode() {
    stage("Checking out code repository") {
        checkout scm
    }
}


def checkoutBranch(BRANCH_NAME) {
    stage("Checking out to branch") {
        sh "git checkout ${BRANCH_NAME}"
    }
}


def commitDetails(){
    def commit_detail = [:]
    commiter_name = sh(script: "git log -1 --pretty=format:'%cn'", returnStdout: true)
    commit_detail.put("commiter_name", commiter_name)
    commit_msg = sh(script: "git log -1 --pretty=format:'%s'", returnStdout: true)
    commit_detail.put("commit_msg", commit_msg)
    return commit_detail
}

def commitDetailStage(){
    def commit_detail = [:]
    try {
        stage("Reading last commit details"){
            commiter_name = sh(script: "git log -1 --pretty=format:'%cn'", returnStdout: true)
            commit_detail.put("commiter_name", commiter_name)
            commit_msg = sh(script: "git log -1 --pretty=format:'%s'", returnStdout: true)
            commit_detail.put("commit_msg", commit_msg)
        }
    }
    catch(Exception e) {
        notification.sendErrorInfo(
            slack_channel: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}",
            color: "danger",
            message: "Job failed while reading commit details",
            error: e
        )
        throw e
        echo e.toString()
    }
    return commit_detail
}


def promoteToGitHub(Map stepParams){
    if ("${stepParams.github_promote}" == "null") {
        if ("${env.BRANCH_NAME}" == "master") {
            pushToGit(
                branch: "${env.BRANCH_NAME}",
                github_url: "${stepParams.github_url}",
                token: "${stepParams.token}",
                token_user: "${stepParams.token_user}",
                slackChannelName: "${stepParams.slackChannelName}",
                environment: "${stepParams.environment}"
            )
        }
    }
    else if ("${stepParams.github_promote}" == "true") {
        pushToGit(
            branch: "${env.BRANCH_NAME}",
            github_url: "${stepParams.github_url}",
            token: "${stepParams.token}",
            token_user: "${stepParams.token_user}",
            slackChannelName: "${stepParams.slackChannelName}",
            environment: "${stepParams.environment}"
        )
    }
    else{
        return "not allow to promote"
    }
    return "success"
}


def pushToGit(Map stepParams){
    try{
        stage ('Add Repo to Github'){
            withCredentials([string(credentialsId: stepParams.token, variable: 'token')]) {
                sh  "git push --force https://${stepParams.token_user}:$token@${stepParams.github_url} HEAD:${stepParams.branch}"
            }
       }
        
    }
    catch(Exception e) {
            notification.sendErrorInfo(
                slack_channel: "${stepParams.slackChannelName}",
                environment: "${stepParams.environment}",
                color: "danger",
                message: "Job fail to sync repository at :- https://${stepParams.github_url}",
                error: e
            )
            throw e
            echo e.toString()
    }
}