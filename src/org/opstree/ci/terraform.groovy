package org.opstree.ci

def executeOperation(Map stepParams) {  
        dir("${stepParams.codePath}") {
        sh "docker run  --rm -v ~/.aws:/root/.aws -v ${WORKSPACE}/${stepParams.codePath}:/workdir nehasinha2011/terraform:1.1 ${stepParams.operation}"
    }

}
