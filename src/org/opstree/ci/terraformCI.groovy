package org.opstree.ci

import org.opstree.commonutilities.*

def call(Map stepParams) {
vcsutils = new org.opstree.commonutilities.vcsUtils()
commonutils =new org.opstree.commonutilities.commonUtils()
terraformop = new terraform()
notificationslack = new org.opstree.commonutilities.notification()
try {
        vcsutils.checkoutCode()
        
    } catch (Exception e) {
        echo "Failed while clonning the codebase"
        echo e.toString()
        throw e
    }


try {
        config = commonutils.readPropertyFile(
            configFilePath: "${stepParams.configFilePath}"
        )
    } catch (Exception e) {
        echo "Failed while reading config file"
        echo e.toString()
        throw e
    }
try
    {
        lintTerraformCode(
            codeBasePath: "${config.CODE_BASE_PATH}"
        )
    } 
catch (Exception e) {
        echo "Failed while linting code"
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while linting code"
        )
        echo e.toString()
        throw e
    }

 try {
        initializeterraforms(
            codeBasePath: "${config.CODE_BASE_PATH}",
            extraArguments: "${config.EXTRA_ARGS}"
        )
    } catch (Exception e) {
        echo "Failed while initializing terraform modules"
        
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while initializing terraform modules"
        )
        echo e.toString()
        throw e
    }   
 try {
        planInfrastructure(
            codeBasePath: "${config.CODE_BASE_PATH}",
            extraArguments: "${config.EXTRA_ARGS}",
            actionMessage: "${config.ACTION_MESSAGE}"
        )
    } catch (Exception e) {
        echo "Failed while planning infrastructure"
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while planning ${config.ACTION_MESSAGE}"
        )
        echo e.toString()
        throw e
    }  
if("${config.Infracost}" == "true")
{
    try {
         estimatingcost(codeBasePath: "${config.CODE_BASE_PATH}")
       
    } 

    catch (Exception e) {
        echo "Failed while estimation infrastructure cost"
        sendClusterFailNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Failed while estimating infra cost ${config.ACTION_MESSAGE}"
        )
        echo e.toString()        
        throw e
    }  
 }
 if("${config.KEEP_APPROVAL_STAGE}" == "true" || "${config.KEEP_APPROVAL_STAGE}" == "null") {
            commonutils.approvalStep()
        }

        try {
            createInfrastructure(
                codeBasePath: "${config.CODE_BASE_PATH}",
                extraArguments: "${config.EXTRA_ARGS}",
                actionMessage: "${config.ACTION_MESSAGE}"
            )
        } catch (Exception e) {
            echo "Failed while creating infrastructure"
            sendClusterFailNotification(
                channelName: "${config.SLACK_CHANNEL_NAME}",
                environment: "${config.ENVIRONMENT}",
                message: "Failed while applying ${config.ACTION_MESSAGE}"
            )
            echo e.toString()
            throw e
        }
     
   sendClusterSuccessNotification(
            channelName: "${config.SLACK_CHANNEL_NAME}",
            environment: "${config.ENVIRONMENT}",
            message: "Successfully applied ${config.ACTION_MESSAGE}"   
        )



}
    def lintTerraformCode(Map stepParams) {
    stage("Linting terraform code") {
        terraformop.executeOperation(
            codePath: "${config.CODE_BASE_PATH}",
            operation: "fmt -list=true -write=false -diff=true"
        )
    }
}
def sendClusterSuccessNotification(Map stepParams) {
    stage("Sending success notification on slack") {
        notificationslack.sendSlackNotification(
            slackChannel: "${stepParams.channelName}",
            buildStatus: "good",
            environment: "${config.ENVIRONMENT}",
            message: "${stepParams.message}"
        )
    }
}

def sendClusterFailNotification(Map stepParams) {
    stage("Sending failure notification on slack") {
        notificationslack.sendSlackNotification(
            slackChannel: "${stepParams.channelName}",
            buildStatus: "danger",
            environment: "${config.ENVIRONMENT}",
            message: "${stepParams.message}"
        )
    }
}

def initializeterraforms(Map stepParams) {
    stage("Initializing the terraform modules") {
        if("${stepParams.extraArguments}" != "null") {
            terraformop.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "init -no-color ${stepParams.extraArguments}"
            )
        } else {
            terraformop.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "init -no-color"
            )
        }
    }
}

def planInfrastructure(Map stepParams) {
    stage("Planning ${stepParams.actionMessage}") {
        if("${stepParams.extraArguments}" != "null") {
            terraformop.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "plan ${stepParams.extraArguments}"
            )
        } else {
            terraformop.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "plan"
            )
        }
    }
}
def estimatingcost(Map stepParams)
{   stage("estimating infra cost ")
  {
      dir("${stepParams.codeBasePath}") {
       sh 'infracost breakdown --path .'
  }
  }
}

def createInfrastructure(Map stepParams) {
    stage("Applying ${stepParams.actionMessage}") {
        if("${stepParams.extraArguments}" != "null") {
            terraformop.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "destroy -auto-approve ${stepParams.extraArguments}"
            )
        } else {
            terraformop.executeOperation(
                codePath: "${config.CODE_BASE_PATH}",
                operation: "destroy -auto-approve"
            )
        }
    }
}





