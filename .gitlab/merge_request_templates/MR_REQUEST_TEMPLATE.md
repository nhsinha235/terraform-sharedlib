## What does this MR do?

<!-- Briefly describe what this MR is about -->

## Have you done this?

- [ ] Added workflow documentation in [docs](../../docs)
- [ ] Updated README with latest information
- [ ] Tested your shared library
- [ ] Updated changelog with all information [CHANGELOG.md](../../CHANGELOG.md)
