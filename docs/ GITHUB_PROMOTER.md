## Ansible Workflow

I am Github promoter library which will sync gitlab repository with github.

### Prerequisites

- [Slack Notification](https://plugins.jenkins.io/slack/) ---> Slack notification plugin should be installed otherwise it will result in job failure.


### Example

```groovy
@Library('common-library@master')

node{
    githubpromoterworkflow.call(
        configFilePath: "/dummy/config.properties"
    )
}
```

### Parameters

|**Paramter Name**| **Type** | **Description** |
|-----------------|----------|-----------------|
| REPO | *mandatory* | Repository name |
| GITHUB_TOKEN_NAME | *mandatory* | You have to define jenkins token credential for github |
| GITHUB_TOKEN_USER | *mandatory* | You have to define jenkins token credential for github |
| PROMOTE | *optional* | Boolean value, set true to publish at github |
| GITHUB_URL | *mandatory* | Gitlab repository url |
| SLACK_CHANNEL_NAME | *manadatory* | Name of the slack channel in which notification should be sent |
| ENVIRONMENT | *mandatory* | Environment name for which pipeline is executing |

### Property File Example

An example property file will look like this:-

```properties
SLACK_CHANNEL_NAME  = osm
ENVIRONMENT         = prod
REPO                = Jenkins
GITHUB_TOKEN_USER   = scuzzlebuzzle
PROMOTE             = true
GITHUB_TOKEN_NAME   = github_token
GITHUB_URL          = github.com/OT-OSM/jenkins.git
```
