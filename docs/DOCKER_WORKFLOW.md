## Docker Workflow

I am docker workflow library which will execute and manage all docker related operation.

### Prerequisites

- [Docker](https://www.docker.com/) ---> Docker should be installed on the system
- [Slack Notification](https://plugins.jenkins.io/slack/) ---> Slack notification plugin should be installed
- [Docker Plugin](https://plugins.jenkins.io/docker-plugin/) ---> Docker jenkins plugin should be installed
- [Aqua MicroScanner](https://plugins.jenkins.io/aqua-microscanner/) ---> Aqua microscanner jenkins plugin should be installed
- [HTML Publisher](https://plugins.jenkins.io/htmlpublisher/) ---> HTML Publisher plugin should be installed


```groovy
@Library('common-library@master')

node{
    dockerWorkflow.call(
        configFilePath: "/dummy/config.properties"
    )
}
```

### Parameters

|**Paramter Name**| **Type** | **Description** |
|-----------------|----------|-----------------|
| SLACK_CHANNEL_NAME | *manadatory* | Name of the slack channel in which notification should be sent |
| ENVIRONMENT | *mandatory* | Environment name for which pipeline is executing |
| IMAGE_NAME | *mandatory* | Name of the docker image |
| IMAGE_VERSION | *mandatory* | Version of the docker image |
| DOCKERFILE_PATH | *mandatory* | Location of the path where Dockerfile is present |
| CHECK_LINT | *optional* | Whether you want to enable the Dockerfile linting or not |
| SCAN_IMAGE | *optional* | Whether you want to enable docker image scanning or not |
| PUBLISH_DOCKERHUB | *optional* | Whether you want to publish image on dockerhub or not |
| PUBLISH_QUAY | *optional* | Whether you want to publish image on quay or not |
| DOCKERHUB_USERNAME | *mandatory* | Username or organization of the dockerhub |
| DOCKERHUB_CREDENTIAL | *mandatory* | Jenkins credential to publish image on dockerhub |
| QUAY_USERNAME | *mandatory* | Username or organization of the quay |
| QUAY_URL | *mandatory* | Self hosted or Saas based quay registry url |
| QUAY_CREDENTIAL | *mandatory* | Jenkins credential to publish image on quay |

### Property File Example

An example property file will look like this:-

```properties
IMAGE_NAME           = restic
IMAGE_VERSION        = 0.1
DOCKERFILE_PATH      = Dockerfile
ENVIRONMENT          = prod
SLACK_CHANNEL_NAME   = docker
CHECK_LINT           = true
SCAN_IMAGE           = true
PUBLISH_DOCKERHUB    = true
PUBLISH_QUAY         = true
DOCKERHUB_USERNAME   = opstreedevops
DOCKERHUB_CREDENTIAL = opstree-docker
QUAY_USERNAME        = opstree
QUAY_URL             = quay.io
QUAY_CREDENTIAL      = opstree-quay
```

### Pipeline View

![](./img/docker-workflow.png)
