## Terraform Workflow

I am terraform workflow library which will execute and manage all terraform related operation.

### Prerequisites

- [Terraform](https://www.terraform.io/) ---> Terraform should be installed on the master or slave from where you are executing the code.
- [Slack Notification](https://plugins.jenkins.io/slack/) ---> Slack notification plugin should be installed otherwise it will result in job failure.

### Example

```groovy
@Library('common-library@master')

node{
    terraformWorkflow.call(
        configFilePath: "/dummy/config.properties"
    )
}
```

### Parameters

|**Paramter Name**| **Type** | **Description** |
|-----------------|----------|-----------------|
| CODE_BASE_PATH | *mandatory* | Path of the terraform codebase which needs to be execute |
| EXTRA_ARGS | *optional* | Extra arguments to terraform if needed to pass |
| TEST_CASE_PATH | *optional* | Path of inpec test cases, if it is written |
| SLACK_CHANNEL_NAME | *manadatory* | Name of the slack channel in which notification should be sent |
| ENVIRONMENT | *mandatory* | Environment name for which pipeline is executing |
| ENABLE_TEST_CASE | *optional* | Whether you want to enable the test case or not |
| KEEP_APPROVAL_STAGE | *optional* | Whether you want to keep the approval step or not |
| ACTION_MESSAGE | *Mandatory* | Action that you are performing with terraform |

### Property File Example

An example property file will look like this:-

```properties
SLACK_CHANNEL_NAME  = build-status
ENVIRONMENT         = prod
CODE_BASE_PATH      = env/prod/
TEST_CASE_PATH      = tests/env/prod/
EXTRA_ARGS          = -var client_secret=xxxxxxx
ENABLE_TEST_CASE    = true
ACTION_MESSAGE      = Whitelisting of IP
KEEP_APPROVAL_STAGE = true
```

### Pipeline View

![](./img/terraform-workflow.png)
