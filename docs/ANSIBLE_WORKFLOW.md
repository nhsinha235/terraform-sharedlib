## Ansible Workflow

I am Ansible workflow library which will execute and manage all Ansible related operation.

### Prerequisites

- [Ansible](https://www.ansible.com/) ---> Ansible should be installed on the master or slave from where you are executing the code.
- [Slack Notification](https://plugins.jenkins.io/slack/) ---> Slack notification plugin should be installed otherwise it will result in job failure.
- [ansible-lint](https://github.com/ansible/ansible-lint) ---> ansible-lint checks playbooks for practices and behaviour that could potentially be improved.
- [ansible-lint-junit](https://github.com/wasilak/ansible-lint-junit) ---> ansible-lint to JUnit converter to publish lint report 

### Example

```groovy
@Library('common-library@master')

node{
    ansibleWorkflow.call(
        configFilePath: "/dummy/config.properties"
    )
}
```

### Parameters

|**Paramter Name**| **Type** | **Description** |
|-----------------|----------|-----------------|
| CODE_BASE_PATH | *mandatory* | Path of the terraform codebase which needs to be execute |
| EXTRA_ARGS | *optional* | Extra arguments to terraform if needed to pass |
| INVENTORY | *mandatory* | Path of Inventory |
| SLACK_CHANNEL_NAME | *manadatory* | Name of the slack channel in which notification should be sent |
| ENVIRONMENT | *mandatory* | Environment name for which pipeline is executing |
| PLAYBOOK_NAME | *mandatory* | Define name of playbook |
| LINTING | *optional* | Whether you want to check linting of your code |
| ACTION_MESSAGE | *Mandatory* | Action that you are performing with terraform |
| BRANCH | *Mandatory* | You have to define branch name to exicute code |

### Property File Example

An example property file will look like this:-

```properties
SLACK_CHANNEL_NAME  = Jenkins
ENVIRONMENT         = prod
CODE_PATH           = path/to/playbook
PLAYBOOK_NAME       = nexus.yml
INVENTORY           = path/to/inventory
ACTION_MESSAGE      = Install Nexus
EXTRA_ARGS          = -e parameter=some_value
BRANCH              = master
LINTING             = true
```

### Pipeline View

![](./img/Ansible-workflow.png)
