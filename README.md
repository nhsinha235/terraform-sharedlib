## CommonLibrary

A Jenkins shared library which will support all kind of operation accross the clients and environments. In this library we support CI/CD for different langauges like PHP, Java, Golang, NodeJS and Python.

In addition, this library also have the capability to support orchestration tools like:-
- Terraform
- Packer
- Helm
- Kubectl
- Inspec
- Ansible
- Docker

### Prerequisites

The jenkins version should be > 2.204 and its recommended to use the latest version. In addtion there are some common plugins which is required.

- [Pipeline](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin)
- [Pipeline Utility Steps](https://wiki.jenkins.io/display/JENKINS/Pipeline+Utility+Steps+Plugin)
- [BlueOcean](https://wiki.jenkins.io/display/JENKINS/Blue+Ocean+Plugin)
- [Pipeline Libraries](https://wiki.jenkins.io/display/JENKINS/Pipeline+Shared+Groovy+Libraries+Plugin)

### Folder Structure

The folder structure of library is:-

```
.
├── docs  ---> For library related documents
└── vars  ---> Library codebase
```

If you want to see the detail documentation for a specific workflow, please visit [doc](./doc)

### Importing Shared Library

- Import Library from **Configure System** in Jenkins

![](./doc/static/global-pipeline-import.png)

- Use this token to import library

```properties
USER = opstree-git-bot
TOKEN = dA4TNCs2h5zA_3zHrWZ2
```

### To Do
- Add Test cases for libraries
